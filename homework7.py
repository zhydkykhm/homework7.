def list_even_odd(list_):
    list_eo = []
    for index, value in enumerate(list_):
        if index % 2 != 0:
            list_eo.append(value[::-1])
        else:
            list_eo.append(value)
    return list_eo


my_list = ['qwe', 'Asd', 'zxc', 'poi', 'laj', 'Anb']
print('1. поворот строк:', list_even_odd(my_list))


def list_a_first(list_):
    list_a = []
    for value in list_:
        for letter in value[0]:
            if letter.lower() == 'a':
                list_a.append(value)
    return list_a


print('2. первая буква а:', list_a_first(my_list))


def list_a_all(list_):
    list_all = []
    for value in list_:
        for letter in value:
            if letter.lower() == 'a':
                list_all.append(value)
    return list_all


print('3. все буквы а:', list_a_all(my_list))


def list_str(list_):
    list_str_new = []
    for value in list_:
        if type(value) == str:
            list_str_new.append(value)
    return list_str_new


my_list_1 = [1004, 'qwe', 22, 'Asd', 'zxc', 125, 78, 'poi', 'laj', 'Anb']
print('4. все строки:', list_str(my_list))


def string_of_rare_letters(str_):
    single_symbols = []
    for symbol in set(str_):
        if str_.count(symbol) == 1:
            single_symbols.append(symbol)
    return single_symbols


my_str = 'Qhellojjkk'
print('5. буквы один раз:', string_of_rare_letters(my_str))


def in_both_lines(str_1, str_2):
    my_str_new = []
    for symbol in str1_:
        if symbol in my_str_new:
            continue
        for symbol_ in str2_:
            if symbol == symbol_:
                my_str_new.append(symbol)
                break
    return my_str_new


str1_ = "Hello"
str2_ = "Hi world"
print('6. символ в обеих строках:', in_both_lines(str1_, str2_))


def one_at_a_time(str_1, str_2):
    single_symbols = []
    for symbol in str1_ :
        sub = str1_.find(symbol) - str1_.rfind(symbol)
        if sub == 0:
            if symbol in str2_ and str2_.find(symbol) - str2_.rfind(symbol) == 0:
                single_symbols.append(symbol)
    return single_symbols


str1_ = "Ффад"
str2_ = "Фллд"
print('7. символ по одному разу:', one_at_a_time(str1_, str2_))


def random_mail(names, domains):
    from random import randint
    import random
    rand_num = random.randint(100, 1000)
    rand_word = ''.join(chr(randint(ord('a'), ord('z'))) for j in range(randint(5, 7)))
    email = random.choice(names) + chr(46) + str(rand_num) + "@" + str(rand_word) + chr(46) + random.choice(domains)
    return email


names = ["Sergei", "Ivan", "Peter"]
domains = ["net", "com", "ua", "ua"]
print('8. случайный mail:', random_mail(names, domains))